import React, {Component} from 'react';
import {Image, Button, TouchableOpacity} from 'react-native';
import { createBottomTabNavigator, createAppContainer,createStackNavigator} from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faSearch, faMusic ,faPlay, faChartBar, faCog } from '@fortawesome/free-solid-svg-icons';

import Home from './Screen/Home';
import BXH from './Screen/BXH';
import NowPlaying from './Screen/NowPlaying';
import Menu from './Screen/Menu';

import Search from './Screen/Search';

//Preset

class Logo extends Component {
    render(){
        return(
            <Image
                source={{uri: 'https://chiasenhac.vn/imgs/logo-web-official.png'}}
                style={{width: 102, height: 37}}
                resizeMode='contain'
            />
        )
    }
}

const appHeader = {
    //headerTitle: <Logo />,
    title: 'Nhạc Số',
    headerTintColor: '#fff',
    headerStyle: {
        backgroundColor: '#000'
    },
    headerRight: (
        <FontAwesomeIcon icon={faSearch} color={ 'white' } size={ 28 }/>
    ),
    headerLeft: (
        <TouchableOpacity onPress={this.props.navigation.push('Search')}>
            <FontAwesomeIcon icon={faCog} color={ 'white' } size={ 28 }/>
        </TouchableOpacity>
    )
}

//Stack Home
export const HomeStack = createStackNavigator({
    Screen_Home : {
        screen: Home,
        navigationOptions: appHeader
    },

    Screen_Search : {
        screen: Search
    },

},{
    initialRouteName: 'Screen_Home',
    headerLayoutPreset: 'center'
});

//Stack BXH

export const BXHStack = createStackNavigator({
    Screen_BXH : {
        screen: BXH,
        navigationOptions: appHeader
    },

    Screen_Search : {
        screen: Search
    },

},{
    initialRouteName: 'Screen_BXH',
    headerLayoutPreset: 'center'
});

//Stack NP

export const NowPlayingStack = createStackNavigator({
    Screen_NowPlaying : {
        screen: NowPlaying,
        navigationOptions: appHeader
    },

    Screen_Search : {
        screen: Search
    },

},{
    initialRouteName: 'Screen_NowPlaying',
    headerLayoutPreset: 'center'
});

//Tab Setting

const TabNavigator = createBottomTabNavigator({
    Home: HomeStack,
    BXH: BXHStack,
    NowPlaying: NowPlayingStack
}, {
    tabBarOptions: {
        activeTintColor: 'white',
        inactiveTintColor: 'black',
        activeBackgroundColor: '#FF2E55',
        inactiveBackgroundColor: '#FB6B60'
    },
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            let iconName;
            if (routeName === 'Home') {
                return <FontAwesomeIcon icon={faMusic} size={ 25 } color={ 'white' }/>;
            } else if (routeName === 'BXH') {
                return <FontAwesomeIcon icon={faChartBar} size={ 25 } color={ 'white' }/>;
            }else if (routeName === 'NowPlaying') {
                return <FontAwesomeIcon icon={faPlay} size={ 25 } color={ 'white' }/>;
            }
        },
    }),

});

export default createAppContainer(TabNavigator);
