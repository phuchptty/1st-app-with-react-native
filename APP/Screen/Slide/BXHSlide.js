import React, {Component} from 'react';
import {View, Text, Button, Image, Dimensions,StyleSheet} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCrown} from '@fortawesome/free-solid-svg-icons';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class BXHSlide extends Component {

    _renderItem ({item, index}) {
        if (index == 0){
            a = <FontAwesomeIcon icon={faCrown} color={ 'yellow' } size={ 25 }/>;
        }else{
            a = "";
        }

        return (
            <View style={{marginTop: 12}}>
                <Text style={style.textTop}>
                    TOP: {index + 1} {a}
                </Text>
                <Image source={{uri: item.cover_html}} style={style.image} />
                <Text>{ item.music_title }</Text>
                <Text style={style.artis}>{ item.music_artist }</Text>
            </View>
        );
    }

    render () {
        return (
            <Carousel
                ref={(c) => { this._carousel = c; }}
                data={this.props.data}
                renderItem={this._renderItem}
                sliderWidth={width}
                itemWidth={ (width * 0.6)}
                layout={'default'}
                firstItem={0}
                autoplay={true}
                loop={true}
            />
        );
    }
}

var style = StyleSheet.create({
    image : {
        width : 170,
        height : 170,
    },

    artis : {
        color: 'red'
    },

    textTop : {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 18,
        color: 'red',
    }
});
