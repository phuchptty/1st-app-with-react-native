import React, {Component} from 'react';
import {ScrollView , Text, Button, StyleSheet,Dimensions, View} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Carousel from 'react-native-snap-carousel';
import AlbumHot from './Slide/AlbumHot';
import AlbumNew from './Slide/AlbumNew';
import MusicNew from './Slide/MusicNew';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowRight, faFire, faCompactDisc, faMusic } from '@fortawesome/free-solid-svg-icons';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class Home extends Component {

    state = {
        data : [],
        areLoading : true
    }

    async componentDidMount() {
        //Get Data
        try {
            let response = await fetch('https://chiasenhac.vn/api/home');
            let responseJson = await response.json();
            //let data = await responseJson.data;
            //console.log(responseJson);
            this.setState({
                data : responseJson.data,
                areLoading : false,
            });

        } catch (error) {
            console.error('ERROR: ----------------> ' + error);
        }
    }

    render(){

        return(
            <ScrollView style={style.container}>
                <Spinner
                    visible={this.state.areLoading}
                    textContent={'Loading...'}
                />

                <View style={style.cardItem}>
                    <View style={style.cardHeader} >
                        <Text style={style.cardHeaderText}><FontAwesomeIcon icon={faFire} color={ '#f3f3f3' } size={ 15 }/> Album Hot</Text>
                        <Text style={style.cardHeaderRightText}>Xem Thêm <FontAwesomeIcon icon={faArrowRight} color={ '#f3f3f3' } size={ 15 }/></Text>
                    </View>
                    <AlbumHot data={this.state.data.album_hot_home}/>
                </View>

                <View style={style.cardItem2nd}>
                    <View style={style.cardHeader} >
                        <Text style={style.cardHeaderText}><FontAwesomeIcon icon={faCompactDisc} color={ '#f3f3f3' } size={ 15 }/> Album Mới</Text>
                        <Text style={style.cardHeaderRightText}>Xem Thêm <FontAwesomeIcon icon={faArrowRight} color={ '#f3f3f3' } size={ 15 }/></Text>
                    </View>
                    <AlbumNew data={this.state.data.album_new_home}/>
                </View>

                <View style={style.cardItem2nd}>
                    <View style={style.cardHeader} >
                        <Text style={style.cardHeaderText}><FontAwesomeIcon icon={faMusic} color={ '#f3f3f3' } size={ 15 }/> Bài Hát Mới</Text>
                        <Text style={style.cardHeaderRightText}>Xem Thêm <FontAwesomeIcon icon={faArrowRight} color={ '#f3f3f3' } size={ 15 }/></Text>
                    </View>
                    <AlbumNew data={this.state.data.music_new_home}/>
                </View>

            </ScrollView>
        );

    }

}

var style = StyleSheet.create({
    container : {
        backgroundColor: '#f3f3f3',
        flex: 1
    },

    cardItem : {
        backgroundColor: '#fff',
        shadowColor: "#2E272B",
        shadowOffset: { width: 2, height: 5},
        shadowOpacity: 1,
        shadowRadius: 6.27,
        elevation: 3,
        marginTop: 5,
        //padding: 10,
        paddingBottom: 10
    },

    cardItem2nd : {
        backgroundColor: '#fff',
        shadowColor: "#2E272B",
        shadowOffset: { width: 2, height: 5},
        shadowOpacity: 1,
        shadowRadius: 6.27,
        elevation: 3,
        marginTop: 15,
        //padding: 10,
        paddingBottom: 10,
    },

    cardHeader : {
        backgroundColor: '#FB6B60',
        width,
        paddingBottom: 8,
        paddingTop: 8,
        paddingLeft: 4,
        //justifyContent: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    cardHeaderText : {
        fontSize: 18,
        //padding: 3,
        color: '#f3f3f3',
        marginLeft: 7
    },

    cardHeaderRightText : {
        fontSize: 18,
        marginRight: 7,
        color: '#f3f3f3',
    }

})
