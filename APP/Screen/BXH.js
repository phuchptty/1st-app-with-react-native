import React, {Component} from 'react';
import {ScrollView , Text, Button, StyleSheet,Dimensions, View} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Carousel from 'react-native-snap-carousel';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChartBar} from '@fortawesome/free-solid-svg-icons';
import BXHSlide from './Slide/BXHSlide';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class BXH extends Component {

    state = {
        vnData : [],
        usukData : [],
        cnData : [],
        krData : [],
        jpData : [],
        areLoadingBXH : true
    }

    async componentDidMount() {
        //Get Data
        try {
            let response = await fetch('https://chiasenhac.vn/api/nhac-hot.html');
            let responseJson = await response.json();
            this.setState({
                vnData : responseJson.data.dataBxh[3],
                usukData : responseJson.data.dataBxh[4],
                cnData : responseJson.data.dataBxh[5],
                krData : responseJson.data.dataBxh[6],
                jpData : responseJson.data.dataBxh[7],
                areLoadingBXH : false,
            });

            //console.log(this.state.data[3].music);

        } catch (error) {
            console.error('ERROR BXH: ----------------> ' + error);
        }
    }

    render(){
        return(

            <ScrollView style={style.container}>

                <Spinner
                    visible={this.state.areLoadingBXH}
                    textContent={'Đang Tải...'}
                />

                <View style={style.cardItem}>
                    <View style={style.cardHeader} >
                        <Text style={style.cardHeaderText}><FontAwesomeIcon icon={faChartBar} color={ '#f3f3f3' } size={ 15 }/> BXH Việt Nam</Text>
                    </View>
                    <BXHSlide data={this.state.vnData.music}/>
                </View>

                <View style={style.cardItem2nd}>
                    <View style={style.cardHeader} >
                        <Text style={style.cardHeaderText}><FontAwesomeIcon icon={faChartBar} color={ '#f3f3f3' } size={ 15 }/> BXH US - UK</Text>
                    </View>
                    <BXHSlide data={this.state.usukData.music}/>
                </View>

                <View style={style.cardItem2nd}>
                    <View style={style.cardHeader} >
                        <Text style={style.cardHeaderText}><FontAwesomeIcon icon={faChartBar} color={ '#f3f3f3' } size={ 15 }/> BXH Nhạc Hoa</Text>
                    </View>
                    <BXHSlide data={this.state.cnData.music}/>
                </View>

                <View style={style.cardItem2nd}>
                    <View style={style.cardHeader} >
                        <Text style={style.cardHeaderText}><FontAwesomeIcon icon={faChartBar} color={ '#f3f3f3' } size={ 15 }/> BXH Nhạc Hàn</Text>
                    </View>
                    <BXHSlide data={this.state.krData.music}/>
                </View>

                <View style={style.cardItem2nd}>
                    <View style={style.cardHeader} >
                        <Text style={style.cardHeaderText}><FontAwesomeIcon icon={faChartBar} color={ '#f3f3f3' } size={ 15 }/> BXH Nhạc Nhật</Text>
                    </View>
                    <BXHSlide data={this.state.jpData.music}/>
                </View>

            </ScrollView>
        )
    }
}


var style = StyleSheet.create({
    container : {
        backgroundColor: '#f3f3f3',
        flex: 1
    },

    cardItem : {
        backgroundColor: '#fff',
        shadowColor: "#2E272B",
        shadowOffset: { width: 2, height: 5},
        shadowOpacity: 1,
        shadowRadius: 6.27,
        elevation: 3,
        marginTop: 5,
        //padding: 10,
        paddingBottom: 10
    },

    cardItem2nd : {
        backgroundColor: '#fff',
        shadowColor: "#2E272B",
        shadowOffset: { width: 2, height: 5},
        shadowOpacity: 1,
        shadowRadius: 6.27,
        elevation: 3,
        marginTop: 15,
        //padding: 10,
        paddingBottom: 10,
    },

    cardHeader : {
        backgroundColor: '#FB6B60',
        width,
        paddingBottom: 8,
        paddingTop: 8,
        paddingLeft: 4,
        //justifyContent: 'center',
    },

    cardHeaderText : {
        fontSize: 18,
        //padding: 3,
        color: '#f3f3f3',
        marginLeft: 7
    },

})
