import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';

export default class Home extends Component {

    render(){
        return(
            <View>
                <Text>ABC</Text>
                <Button
                    title="Go Back"
                    onPress={() => this.props.navigation.goBack()}
                />
            </View>
        )
    }
}
